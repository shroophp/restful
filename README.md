# ShrooPHP\RESTful

A PHP library for building RESTful applications.

## Installation

```
composer require 'shroophp/restful ^1.0'
```

## Example Usage

```php
<?php

use ShrooPHP\Framework\Application;
use ShrooPHP\RESTful\Collections\HashTable;
use ShrooPHP\RESTful\Resources\Immutable;
use ShrooPHP\RESTful\Request\Handler;

require 'vendor/autoload.php';

$collection = new HashTable;
$handler = new Handler($collection);
$resource = new Immutable('Hello, world!', 'text/plain');

$collection->put('/', $resource);

(new Application)->push($handler)->run();

```

