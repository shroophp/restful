<?php

namespace ShrooPHP\RESTful;

use DateTime;

/**
 * A resource.
 */
interface Resource
{
	/**
	 * Gets the type of the resource.
	 *
	 * @return string|null The type of the resource (or NULL to assume the
	 * default).
	 */
	public function type(): ?string;

	/**
	 * Gets the time at which the resource was last modified.
	 *
	 * @return \DateTime The time at which the resource was last modified.
	 */
	public function modified(): DateTime;

	/**
	 * Gets the size of the resource.
	 *
	 * @return int|null The size of the resource (or NULL if the size is
	 * unknown).
	 */
	public function size(): ?int;

	/**
	 * Outputs the contents of the resource.
	 *
	 * @param int $start The byte at which to begin output.
	 * @param int $length|null The number of remaining bytes to output (or NULL
	 * to output all remaining bytes.
	 */
	public function render(int $start = 0, int $length = null): void;
}
