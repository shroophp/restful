<?php

namespace ShrooPHP\RESTful\Collections;

use ShrooPHP\RESTful\Collection;
use ShrooPHP\RESTful\Collection\Traits\HashTable as HashTableTrait;

/**
 * A collection that stores resources in memory.
 */
class HashTable implements Collection
{
	use HashTableTrait;

	/**
	 * The current maximum length of the underlying hash table (if any).
	 *
	 * @var int|null
	 */
	private $limit;

	/**
	 * Constructs a collection that stores resources in memory
	 *
	 * @param int|null $limit The maximum length of the underlying hash table
	 * (if any).
	 */
	public function __construct(int $limit = null)
	{
		$this->limit = $limit;
	}

	protected function limit(): ?int
	{
		return $this->limit;
	}
}
