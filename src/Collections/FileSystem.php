<?php

namespace ShrooPHP\RESTful\Collections;

use ShrooPHP\RESTful\Collection;
use ShrooPHP\RESTful\Collection\Traits\FileSystem as FileSystemTrait;
use ShrooPHP\RESTful\Resources\File;

/**
 * A collection that persists resources via the file system.
 */
class FileSystem implements Collection
{
	/**
	 * The default size of the buffer.
	 */
	const BUFFER = File::BUFFER;

	use FileSystemTrait;

	/**
	 * The current size of the buffer.
	 *
	 * @var int
	 */
	private $buffer;

	/**
	 * Constructs a collection that persists resources via the file system.
	 *
	 * @param int|null $buffer The size of the buffer (or NULL to use the
	 * default).
	 */
	public function __construct(int $buffer = null)
	{
		$this->buffer = $buffer ?? self::BUFFER;
	}

	protected function buffer(): int
	{
		return $this->buffer;
	}
}
