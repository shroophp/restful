<?php

namespace ShrooPHP\RESTful;

use ShrooPHP\RESTful\Resource;

/**
 * A collection of resources.
 */
interface Collection
{
	/**
	 * Gets the resource with the given ID.
	 *
	 * @param string $id The ID of the resource to retrieve.
	 * @return \ShrooPHP\RESTful\Resource|null The retrieved resource (or NULL
	 * if no resource was found).
	 */
	public function get(string $id): ?Resource;

	/**
	 * Posts the given resource to the given ID.
	 *
	 * @param string $id The ID to post the resource to.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to post to the
	 * ID.
	 * @return string|null The new ID of the given resource (or NULL if no
	 * redirection is necessary).
	 */
	public function post(string $id, Resource $resource): ?string;

	/**
	 * Puts the given resource to the given ID.
	 *
	 * @param string $id The ID to put the resource to.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to put to the
	 * ID.
	 */
	public function put(string $id, Resource $resource): void;

	/**
	 * Patches the resource at the given ID.
	 *
	 * @param string $id The ID of the resource to patch.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to apply as the
	 * patch.
	 */
	public function patch(string $id, Resource $resource): void;

	/**
	 * Deletes the resource with the given ID.
	 *
	 * @param string $id The ID of the resource to delete.
	 */
	public function delete(string $id): void;
}
