<?php

namespace ShrooPHP\RESTful\Collection;

/**
 * An error that relates to an unsupported method being invoked.
 */
interface UnsupportedMethodError
{

}
