<?php

namespace ShrooPHP\RESTful\Collection\Traits;

use ShrooPHP\RESTful\Collection\UnsupportedMethodException;
use ShrooPHP\RESTful\Resource;

/**
 * Functionality for collections that only support a subset of known methods.
 */
trait Unimplemented
{
	public function get(string $id): ?Resource
	{
		throw new UnsupportedMethodException;
	}

	public function post(string $id, Resource $resource): ?string
	{
		throw new UnsupportedMethodException;
	}

	public function put(string $id, Resource $resource): void
	{
		throw new UnsupportedMethodException;
	}

	public function patch(string $id, Resource $resource): void
	{
		throw new UnsupportedMethodException;
	}

	public function delete(string $id): void
	{
		throw new UnsupportedMethodException;
	}
}
