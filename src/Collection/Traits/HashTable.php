<?php

namespace ShrooPHP\RESTful\Collection\Traits;

use ShrooPHP\RESTful\Resource;

/**
 * Functionality for collections that store resources in memory.
 */
trait HashTable
{
	/**
	 * The associative array being used to represent the hash table.
	 *
	 * @var array
	 */
	private $resources = [];

	public function get(string $id): ?Resource
	{
		return $this->resources[$id] ?? null;
	}

	public function post(string $id, Resource $resource): ?string
	{
		$this->set($id, $resource);

		return null;
	}

	public function put(string $id, Resource $resource): void
	{
		$this->set($id, $resource);
	}

	public function patch(string $id, Resource $resource): void
	{
		$this->set($id, $resource);
	}

	public function delete(string $id): void
	{
		unset($this->resources[$id]);
	}

	/**
	 * Gets the current maximum length of the underlying hash table (if any).
	 *
	 * @returns int|null The current maximum length of the underlying hash table
	 * (if any).
	 */
	protected abstract function limit(): ?int;

	/**
	 * Associates the given ID with the given resource in the underlying hash
	 * table.
	 *
	 * @param string $id The ID to associate with the resource.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to associate
	 * with the ID.
	 */
	private function set(string $id, Resource $resource): void
	{
		$limit = $this->limit();
		$this->resources[$id] = $resource;

		if (!is_null($limit) && count($this->resources) > $limit) {
			array_shift($this->resources);
		}
	}
}
