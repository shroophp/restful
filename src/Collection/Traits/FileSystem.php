<?php

namespace ShrooPHP\RESTful\Collection\Traits;

use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resources\File;
use SplFileInfo;
use SplFileObject;

/**
 * Functionality for collections that persist resources via the file system.
 */
trait FileSystem
{
	public function get(string $id): ?Resource
	{
		$resource = null;
		$info = new SplFileInfo($id);

		if ($info->isFile()) {
			$resource = $this->toResource($info);
		}

		return $resource;
	}

	public function post(string $id, Resource $resource): ?string
	{
		$this->write($id, $resource);

		return null;
	}

	public function put(string $id, Resource $resource): void
	{
		$this->write($id, $resource);
	}

	public function patch(string $id, Resource $resource): void
	{
		$this->write($id, $resource);
	}

	public function delete(string $id): void
	{
		unlink($id);
	}

	/**
	 * Gets the size of the buffer.
	 *
	 * @return int The size of the buffer.
	 */
	protected abstract function buffer(): int;

	/**
	 * Converts the given path to a resource.
	 *
	 * @param string $path The path to convert.
	 * @return \ShrooPHP\RESTful\Resource The converted path.
	 */
	private function toResource(string $path): Resource
	{
		return new File($path, $this->buffer());
	}

	/**
	 * Writes the given resource to the given path.
	 *
	 * @param string $path The path to write the resource to.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to be written.
	 */
	private function write(string $path, Resource $resource): void
	{
		$file = new SplFileObject($path, 'wb');
		ob_start(function (string $buffer) use ($file) {
			$file->fwrite($buffer);
			return '';
		}, $this->buffer());
		$resource->render();
		ob_end_clean();
	}
}
