<?php

namespace ShrooPHP\RESTful\Collection;

use Exception;
use ShrooPHP\RESTful\Collection\UnsupportedMethodError as Error;

/**
 * An exception indicating that an unsupported method has been invoked.
 */
class UnsupportedMethodException extends Exception implements Error
{

}
