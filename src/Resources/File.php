<?php

namespace ShrooPHP\RESTful\Resources;

use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resource\Traits\File as FileTrait;

/**
 * A resource that is represented as a file.
 */
class File implements Resource
{
	use FileTrait;

	/**
	 * The default size of the buffer.
	 */
	const BUFFER = 0x1000;

	/**
	 * The path of the file representing the resource.
	 *
	 * @var string
	 */
	private $path;

	/**
	 * The size of the buffer.
	 *
	 * @var int
	 */
	private $buffer;

	/**
	 * Constructs a resource that is represented as a file.
	 *
	 * @param string $path The path to the file representing the resource.
	 * @param int $buffer The size of the buffer (or NULL to use the default).
	 */
	public function __construct(string $path, int $buffer = null)
	{
		$this->path = $path;
		$this->buffer = $buffer ?? self::BUFFER;
	}

	protected function path(): string
	{
		return $this->path;
	}

	protected function buffer(): int
	{
		return $this->buffer;
	}
}
