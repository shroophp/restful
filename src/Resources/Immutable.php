<?php

namespace ShrooPHP\RESTful\Resources;

use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resource\Traits\Binary;
use ShrooPHP\RESTful\Resource\Traits\Touchable;
use RuntimeException;

/**
 * A resource that is represented in memory.
 */
class Immutable implements Resource
{
	/**
	 * The message associated with exceptions indicating that the output buffer
	 * cannot be started.
	 */
	const MESSAGE = 'Cannot start output buffer';

	use Binary, Touchable;

	/**
	 * The contents of the resource.
	 *
	 * @var string
	 */
	private $binary;

	/**
	 * The type of the resource (if any).
	 *
	 * @var string|null
	 */
	private $type;

	/**
	 * Extracts the contents of the given resource.
	 *
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to extract the
	 * contents of.
	 * @param int $start The byte from which to begin the extraction.
	 * @param int|null $length The number of bytes to extract (or NULL to
	 * extract all remaining bytes).
	 * @param bool $error Whether or not to force an error (used for code
	 * coverage purposes).
	 * @return string The extracted contents.
	 * @throws \RuntimeException The output buffer could not be started.
	 */
	public static function toString(
			Resource $resource,
			int $start = 0,
			int $length = null,
			bool $error = false
	): string {

		$string = '';
		$success = false;

		if (!$error) {
			$success = ob_start();
		}

		if (!$success) {
			throw new RuntimeException(self::MESSAGE);
		}

		try {

			$resource->render($start, $length);

		} finally {

			$string .= (string) ob_get_clean();
		}

		return $string;
	}

	/**
	 * Converts the given resource to an immutable resource.
	 *
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to convert.
	 * @param int $start The byte from which to begin the extraction of the
	 * contents.
	 * @param int|null $length The number of bytes to extract from the contents
	 * (or NULL to extract all remaining bytes).
	 * @return \ShrooPHP\RESTful\Resources\Immutable The converted resource.
	 */
	public static function createFromResource(
			Resource $resource,
			int $start = 0,
			int $length = null
	): self {

		$type = $resource->type();
		return new self(self::toString($resource, $start, $length), $type);
	}

	/**
	 * Constructs a resource that is represented in memory.
	 *
	 * @param string $binary The contents of the resource.
	 * @param string $type The type of the resource (if any).
	 */
	public function __construct(string $binary, string $type = null)
	{
		$this->binary = $binary;
		$this->type = $type;
		$this->touch();
	}

	public function type(): ?string
	{
		return $this->type;
	}

	protected function binary(): string
	{
		return $this->binary;
	}
}
