<?php

namespace ShrooPHP\RESTful\Resources;

use DateTime;
use ShrooPHP\Core\Openable;
use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resource\Traits\Pointer as PointerTrait;

/**
 * A resource that is represented as a file pointer.
 */
class Pointer implements Resource
{
	/**
	 * The default size of the buffer.
	 */
	const BUFFER = File::BUFFER;

	use PointerTrait;

	/**
	 * The file pointer representing the resource.
	 *
	 * @var resource
	 */
	private $openable;

	/**
	 * The type of the resource (if any).
	 *
	 * @var string|null
	 */
	private $type;

	/**
	 * The modification time of the resource (if any).
	 *
	 * @var \DateTime|null
	 */
	private $modified;

	/**
	 * The size of the resource (if any).
	 *
	 * @var int|null
	 */
	private $size;

	/**
	 * The size of the buffer.
	 *
	 * @var int
	 */
	private $buffer;

	/**
	 * Constructs a resource that is represented as a file pointer.
	 *
	 * @param \ShrooPHP\Core\Openable $openable The instance that opens the
	 * pointer.
	 * @param string|null $type The type of the resource (if any).
	 * @param \DateTime|null $modified The modification time of the resource (or
	 * NULL to use the default).
	 * @param int|null $size The size of the resource (if any).
	 * @param int|null $buffer The size of the buffer (or NULL to use the
	 * default).
	 */
	public function __construct(
			Openable $openable,
			string $type = null,
			DateTime $modified = null,
			int $size = null,
			int $buffer = null
	) {
		$this->openable = $openable;
		$this->type = $type;
		$this->modified = $modified;
		$this->size = $size;
		$this->buffer = $buffer ?? self::BUFFER;
	}

	public function type(): ?string
	{
		return $this->type;
	}

	public function modified(): DateTime
	{
		return $this->modified ?? new DateTime;
	}

	public function size(): ?int
	{
		return $this->size;
	}

	protected function pointer()
	{
		return $this->openable->open();
	}

	protected function buffer(): int
	{
		return $this->buffer;
	}
}
