<?php

namespace ShrooPHP\RESTful\Resource;

/**
 * An error that relates to an unsupported range being specified.
 */
interface UnsupportedRangeError
{

}
