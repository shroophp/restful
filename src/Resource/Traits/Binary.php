<?php

namespace ShrooPHP\RESTful\Resource\Traits;

/**
 * Functionality for resources with contents that is represented as a string.
 */
trait Binary
{
	public function render(int $start = 0, int $length = null): void
	{
		if (is_null($length)) {
			echo (string) substr($this->binary(), $start);
		} else {
			echo (string) substr($this->binary(), $start, $length);
		}
	}

	public function size(): ?int
	{
		return strlen($this->binary());
	}

	/**
	 * Gets the current contents of the resource.
	 *
	 * @return string The current contents of the resource.
	 */
	protected abstract function binary(): string;
}
