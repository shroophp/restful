<?php

namespace ShrooPHP\RESTful\Resource\Traits;

use DateTime;
use finfo;
use ShrooPHP\RESTful\Resource\Traits\Pointer;
use SplFileInfo;

/**
 * Functionality for resources that are represented as a file.
 */
trait File
{
	use Pointer;

	public function modified(): DateTime
	{
		$info = $this->toSplFileInfo();
		return DateTime::createFromFormat('U', $info->getMTime());
	}

	public function type(): ?string
	{
		$finfo = finfo_open(FILEINFO_MIME);
		$type = finfo_file($finfo, $this->path());
		finfo_close($finfo);

		return is_string($type) ? $type : null;
	}

	public function size(): ?int
	{
		return $this->toSplFileInfo()->getSize();
	}

	/**
	 * Gets the current path to the underlying file.
	 *
	 * @return The current path to the underlying file.
	 */
	protected abstract function path(): string;

	protected function pointer()
	{
		return fopen($this->path(), 'rb');
	}

	/**
	 * Converts the current path to file information.
	 *
	 * @return \SplFileInfo The converted path.
	 */
	private function toSplFileInfo(): SplFileInfo
	{
		return new SplFileInfo($this->path());
	}
}
