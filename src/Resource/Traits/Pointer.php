<?php

namespace ShrooPHP\RESTful\Resource\Traits;

use ShrooPHP\RESTful\Resource\Traits\Pointer\Exception;

/**
 * Functionality for resources that are represented as a file pointer.
 */
trait Pointer
{
	public function render(int $start = 0, int $length = null): void
	{
		$handle = $this->pointer();

		if (!is_resource($handle)) {
			throw new Exception;
		}

		fseek($handle, $start);

		if (is_null($length)) {
			fpassthru($handle);
		} else {
			$this->partial($handle, $length);
		}
	}

	/**
	 * Opens a pointer to the underlying file.
	 *
	 * @return resource|null A pointer to the underlying file (or NULL on
	 * failure).
	 */
	protected abstract function pointer();

	/**
	 * Gets the size of the buffer.
	 *
	 * @return int The size of the buffer.
	 */
	protected abstract function buffer(): int;

	/**
	 * Outputs the given number of bytes (at most) from the given pointer.
	 *
	 * @param resource $handle The pointer to output from.
	 * @param int $length The number of bytes to output (at most).
	 */
	private function partial($handle, int $length): void
	{
		$rendered = 0;

		while (!feof($handle) && $rendered < $length) {
			$buffer = $this->buffer();
			echo (string) substr(fread($handle, $buffer), 0, $length - $rendered);
			$rendered += $buffer;
		}
	}
}
