<?php

namespace ShrooPHP\RESTful\Resource\Traits;

use DateTime;

/**
 * Functionality for resources with modification times that can be updated at
 * runtime.
 */
trait Touchable
{
	private $touched = null;

	public function modified(): DateTime
	{
		return $this->touched ?? $this->now();
	}

	/**
	 * Updates the current modification time with the current time.
	 */
	protected function touch(): void
	{
		$this->touched = $this->now();
	}

	private function now(): DateTime
	{
		return $now = new DateTime;
	}
}
