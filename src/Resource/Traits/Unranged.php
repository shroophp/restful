<?php

namespace ShrooPHP\RESTful\Resource\Traits;

use InvalidArgumentException as Exception;
use ShrooPHP\RESTful\Resource\UnsupportedRangeException;

/**
 * Functionality for resources that do not support ranges.
 */
trait Unranged
{
	public function render(int $start = 0, int $length = null): void
	{
		// If the range is not full...
		if (!is_null($length) || $start != 0) {
			throw new UnsupportedRangeException;
		}

		$this->passthru();
	}

	/**
	 * Outputs the current contents of the resource in its entirety.
	 */
	protected abstract function passthru(): void;
}
