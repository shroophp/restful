<?php

namespace ShrooPHP\RESTful\Resource\Traits\Pointer;

use RuntimeException;

/**
 * An exception indicating a failure to open a pointer.
 */
class Exception extends RuntimeException
{

}
