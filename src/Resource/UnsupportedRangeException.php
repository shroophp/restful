<?php

namespace ShrooPHP\RESTful\Resource;

use Exception;
use ShrooPHP\RESTful\Resource\UnsupportedRangeError as Error;

/**
 * An exception indicating that an unsupported range has been specified.
 */
class UnsupportedRangeException extends Exception implements Error
{

}
