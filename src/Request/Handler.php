<?php

namespace ShrooPHP\RESTful\Request;

use DateTime;
use DateTimeZone;
use EmptyIterator;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Handler as IHandler;
use ShrooPHP\Core\Request\Response as IResponse;
use ShrooPHP\Core\Request\Response\Presenter as IPresenter;
use ShrooPHP\Framework\Request\Response\Presenters\Presenter;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\RESTful\Collection;
use ShrooPHP\RESTful\Collection\UnsupportedMethodError;
use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resource\UnsupportedRangeError;
use ShrooPHP\RESTful\Resources\Immutable;
use ShrooPHP\RESTful\Resources\Pointer;
use ShrooPHP\RESTful\Runnables\Partial;

/**
 * A RESTful request handler.
 */
class Handler implements IHandler
{
	/**
	 * The regular expression for extracting the byte range of a request.
	 */
	const REGEX = '/bytes=\h*(\d+)-(\d*)[\D.*]?/i';

	/**
	 * The collection containing resources.
	 *
	 * @var \ShrooPHP\RESTful\Collection
	 */
	private $collection;

	/**
	 * The presenter being used to present responses.
	 *
	 * @var \ShrooPHP\Core\Request\Response\Presenter;
	 */
	private $presenter;

	/**
	 * Constructs a RESTful request handler.
	 *
	 * @param \ShrooPHP\RESTful\Collection $collection The collection containing
	 * resources.
	 * @param \ShrooPHP\Core\Request\Response\Presenter $presenter The presenter
	 * to use to present responses.
	 */
	public function __construct(
			Collection $collection,
			IPresenter $presenter = null
	) {
		$this->collection = $collection;
		$this->presenter = $presenter ?? new Presenter;
	}

	public function handle(Request $request)
	{
		$handled = false;
		$response = $this->respond($request);

		if (!is_null($response)) {
			$this->presenter->present($response);
			$handled = true;
		}

		return $handled;
	}

	/**
	 * Converts the given request to a response.
	 *
	 * @param \ShrooPHP\Core\Request $request The request to convert.
	 * @return \ShrooPHP\Core\Request\Response|null $response The converted
	 * request (or NULL if the requested resource was not found).
	 */
	public function respond(Request $request): ?IResponse
	{
		$id = null;
		$response = null;
		$resource = new Pointer($request);

		try {

			switch ($request->method()) {
				case 'GET':
					$resource = $this->collection->get($request->path());
					break;
				case 'POST':
					$id = $this->collection->post($request->path(), $resource);
					break;
				case 'PUT':
					$this->collection->put($request->path(), $resource);
					break;
				case 'PATCH':
					$this->collection->patch($request->path(), $resource);
					break;
				case 'DELETE':
					$this->collection->delete($request->path());
					break;
			}

		} catch (UnsupportedMethodError $error) {
			$response = new Response(Response::HTTP_METHOD_NOT_ALLOWED);
		}

		if (is_null($response) && !is_null($resource)) {
			$response = $this->toResponse($request, $resource, $id);
		}

		return $response;
	}

	/**
	 * Converts the request handler to a callable that is compatible as a
	 * request handler callback.
	 *
	 * @return callable The converted request handler.
	 */
	public function toCallable(): callable
	{
		return [$this, 'respond'];
	}

	/**
	 * Converts the given request to a response.
	 *
	 * @param \ShrooPHP\Core\Request $request The request to convert.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource known to be
	 * associated with the request.
	 * @param string $id The ID known to be associated with the request.
	 * @return \ShrooPHP\Core\Request\Response The converted request.
	 */
	private function toResponse(
			Request $request,
			Resource $resource,
			string $id = null
	): Response {

		$start = 0;
		$length = null;
		$since = null;
		$ranged = false;
		$response = new Response;
		$created = !is_null($id);
		$headers = $created ? new EmptyIterator : $request->headers();

		foreach ($headers as $header => $value) {

			switch (strtolower(trim($header))) {
				case 'range':
					$ranged = $ranged || $this->toRange($value, $start, $length);
					break;
				case 'if-modified-since':
					$since = $since ?? $this->toDateTime($value);
					break;
			}

			if ($ranged && $since) {
				break;
			}
		}

		if ($created) {
			$response->setCode(Response::HTTP_CREATED);
			$response->addHeader('Location', $id);
		} else {
			$this->existent($response, $resource, $start, $length, $since);
		}

		return $response;
	}

	/**
	 * Converts the given string to a date and time.
	 *
	 * @param string $since The string to convert (in HTTP date format).
	 * @return \DateTime|null The converted string (or NULL if it could not be
	 * converted.
	 */
	private function toDateTime(string $since): ?DateTime
	{
		return DateTime::createFromFormat($this->toFormat('T'), $since) ?: null;
	}

	/**
	 * Converts the given string to a range of bytes.
	 *
	 * @param string $range The range to convert.
	 * @param int $start The variable to be assigned the start byte.
	 * @param int $length The variable to be assigned the length.
	 * @return bool Whether or not a range was successfully extracted from the
	 * string.
	 */
	private function toRange(
			string $range,
			int &$start,
			int &$length = null
	): bool {

		$end = '';
		$matches = [];
		$matched = preg_match(self::REGEX, $range, $matches);

		if ($matched) {
			$start = (int) ($matches[1] ?? 0);
			$end = $matches[2] ?? '';
		}

		if ($end !== '') {
			$length = ((int) $end) - $start;
		}

		return $matched;
	}

	/**
	 * Modifies the given response with the given resource.
	 *
	 * @param \ShrooPHP\Framework\Request\Responses\Response $response The
	 * response to modify.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to use in order
	 * to modify the response.
	 * @param int $start The start byte to add to the response.
	 * @param int|null $length The length to add to the response (if any).
	 * @param \DateTime|null The last known modification time to evaluate (if
	 * any).
	 */
	private function existent(
			Response $response,
			Resource $resource,
			int $start = 0,
			int $length = null,
			DateTime $since = null
	): void {

		$modified = clone $resource->modified();
		$utc = new DateTimeZone('UTC');
		$format = $this->toFormat('\G\M\T');
		$formatted = $modified->setTimezone($utc)->format($format);

		$response->addHeader('Pragma', 'no-cache');
		$response->addHeader('Cache-Control', 'no-cache');
		$response->addHeader('Last-Modified', $formatted);

		if ($modified > $since) {
			$this->modified($response, $resource, $start, $length);
		} else {
			$response->setCode(Response::HTTP_NOT_MODIFIED);
		}
	}

	/**
	 * Modifies the given response with the given resource and range with the
	 * assumption that the resource has been modified.
	 *
	 * @param \ShrooPHP\Framework\Request\Responses\Response $response The
	 * response to modify.
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to use in order
	 * to modify the response.
	 * @param int $start The start byte to add to the response.
	 * @param int|null $length The length to add to the response (if any).
	 */
	private function modified(
			Response $response,
			Resource $resource,
			int $start = 0,
			int $length = null
	): void {

		$type = $resource->type();
		$size = $resource->size();

		if ($this->isRanged($resource)) {
			$response->addHeader('Accept-Ranges', 'bytes');
		} else {
			$start = 0;
			$length = null;
		}

		$range = $this->toContentRange($size, $start, $length);

		if (!is_null($range)) {
			$response->addHeader('Content-Range', $range);
		}

		if (!is_null($type)) {
			$response->addHeader('Content-Type', $type);
		}

		if (!is_null($length)) {
			$response->addHeader('Content-Length', $length);
		}

		$content = new Partial($resource, $start, $length);
		$response->setContent($content);
	}

	/**
	 * Determines whether or not the given resource accepts ranges.
	 *
	 * @param Resource $resource The resource to evaluate.
	 * @return bool Whether or not the given resource accepts ranges.
	 */
	private function isRanged(Resource $resource): bool
	{

		$ranged = true;

		// Determine if byte ranges are accepted.
		try {
			Immutable::toString($resource, 0, 0);
		} catch (UnsupportedRangeError $error) {
			$ranged = false;
		}

		return $ranged;
	}

	/**
	 * Converts the given resource size, start byte and number of bytes to a
	 * content range header (if any).
	 *
	 * @param int $size The resource size to convert.
	 * @param int $start The start byte to convert.
	 * @param int $length The length to sanitize and evaluate (if any).
	 * @return string|null The content range header (if any).
	 */
	private function toContentRange(
			int $size = null,
			int $start = 0,
			int &$length = null
	): ?string {

		$range = null;
		$end = null;
		$total = (string) ($size ?? '*');

		if (!is_null($length)) {

			$end = $this->toAtLeastZero($start + $length - 1);

			// Amend range if specified length is too long.
			if (!is_null($size) && $end >= $size) {
				$end = $this->toAtLeastZero($size - 1);
				$length = $this->toAtLeastZero($size - $start);
			}

		} else if (!is_null($size)) {

			$end = $this->toAtLeastZero($size - 1);
		}

		if (!is_null($end)) {
			$range = "bytes {$start}-{$end}/{$total}";
		}

		return $range;
	}

	/**
	 * Converts the given integer to zero if it is less than zero.
	 *
	 * Otherwise, no conversion is performed.
	 *
	 * @param int $int The integer to convert.
	 * @return int The converted integer.
	 */
	private function toAtLeastZero(int $int): int {

		if ($int < 0) {
			$int = 0;
		}

		return $int;
	}

	/**
	 * Generates a HTTP date format string.
	 *
	 * @param string $timezone The format of the time zone.
	 * @return string The generated HTTP date format string.
	 */
	private function toFormat(string $timezone): string
	{
		return "D, d M Y H:i:s {$timezone}";
	}
}
