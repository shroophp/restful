<?php

namespace ShrooPHP\RESTful\Runnables;

use ShrooPHP\Core\Runnable;
use ShrooPHP\RESTful\Resource;

/**
 * A runnable instance that outputs part of a resource.
 */
class Partial implements Runnable
{
	/**
	 * The resource being output.
	 *
	 * @var \ShrooPHP\RESTful\Resource
	 */
	private $resource;

	/**
	 * The byte from which output is beginning.
	 *
	 * @var int
	 */
	private $start;

	/**
	 * The number of remaining bytes being output (or NULL if all remaining
	 * bytes are being output).
	 *
	 * @var int|null
	 */
	private $length;

	/**
	 * Constructs a runnable instance that outputs part of the given resource.
	 *
	 * @param \ShrooPHP\RESTful\Resource $resource The resource to output.
	 * @param int $start The byte from which to begin output.
	 * @param int|null $length The number of bytes to output (or NULL to output
	 * all remaining bytes).
	 */
	public function __construct(
			Resource $resource,
			int $start = 0,
			int $length = null
	) {
		$this->resource = $resource;
		$this->start = $start;
		$this->length = $length;
	}

	public function run()
	{
		$this->resource->render($this->start, $this->length);
	}
}
