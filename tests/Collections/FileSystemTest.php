<?php

namespace ShrooPHP\RESTful\Tests\Collections;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use ShrooPHP\RESTful\Collections\FileSystem;
use ShrooPHP\RESTful\Resources\Immutable;
use ShrooPHP\RESTful\Resources\File;

class FileSystemTest extends TestCase
{
	public function testPost()
	{
		$this->assertFileSystem('post');
	}

	public function testPut()
	{
		$this->assertFileSystem('put');
	}
	public function testPatch()
	{
		$this->assertFileSystem('patch');
	}

	private function assertFileSystem(string $method)
	{
		$fs = new FileSystem;
		$root = vfsStream::setup();
		$id = "{$root->url()}/file";
		$expected = new File($id);

		$this->assertNull($fs->get($id));

		[$fs, $method]($id, new File(__FILE__));

		$actual = $fs->get($id);

		$this->assertNotNull($actual);
		$this->assertTrue($expected->modified() == $actual->modified());
		$this->assertEquals($expected->size(), $actual->size());
		$this->assertEquals(Immutable::toString($expected), Immutable::toString($actual));

		$fs->delete($id);

		$this->assertNull($fs->get($id));
	}
}
