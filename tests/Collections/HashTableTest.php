<?php

namespace ShrooPHP\RESTful\Tests\Collections;

use PHPUnit\Framework\TestCase;
use ShrooPHP\RESTful\Collections\HashTable;
use ShrooPHP\RESTful\Resources\Immutable;

class HashTableTest extends TestCase
{
	public function testPost()
	{
		$this->assertHashTable('post');
	}

	public function testPostLimit()
	{
		$this->assertHashTableLimit('post');
	}

	public function testPut()
	{
		$this->assertHashTable('put');
	}

	public function testPutLimit()
	{
		$this->assertHashTableLimit('put');
	}

	public function testPatch()
	{
		$this->assertHashTable('patch');
	}

	public function testPatchLimit()
	{
		$this->assertHashTableLimit('patch');
	}

	public function assertHashTable(string $method)
	{
		$id = '/';
		$collection = new HashTable;
		$resource = new Immutable('');

		$this->assertNull($collection->get($id));

		[$collection, $method]($id, $resource);

		$this->assertSame($resource, $collection->get($id));

		$collection->delete($id);

		$this->assertNull($collection->get($id));
	}

	public function assertHashTableLimit(string $method)
	{
		$one = '/1';
		$two = '/2';
		$collection = new HashTable(1);
		$first = new Immutable('');
		$second = new Immutable('');
		$callback = [$collection, $method];

		$callback($one, $first);

		$this->assertSame($first, $collection->get($one));

		$callback($two, $second);

		$this->assertSame($second, $collection->get($two));
		$this->assertNull($collection->get($one));
	}
}
