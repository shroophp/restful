<?php

namespace ShrooPHP\RESTful\Tests\Resource\Traits;

use PHPUnit\Framework\TestCase;
use ShrooPHP\RESTful\Resource\Traits\Unranged;
use ShrooPHP\RESTful\Resource\UnsupportedRangeError;

class UnrangedTest extends TestCase
{
	public function test()
	{
		$error = null;
		$unranged = new class {

			use Unranged;

			private $rendered = false;

			public function rendered()
			{
				return $this->rendered;
			}

			protected function passthru()
			{
				$this->rendered = true;
			}
		};

		try {
			$unranged->render(0, 0);
		} catch (UnsupportedRangeError $error) {
			// Do nothing (implicitly assign the error).
		}

		$this->assertNotNull($error);

		$unranged->render();

		$this->assertTrue($unranged->rendered());
	}
}
