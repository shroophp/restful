<?php

namespace ShrooPHP\RESTful\Tests\Resources;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resources\File;
use ShrooPHP\RESTful\Resources\Immutable;

class ImmutableTest extends TestCase
{

	public function test()
	{
		$this->assertCreateFromResource();
	}

	public function testWithOffset()
	{
		$this->assertCreateFromResource(5);
	}

	public function testWithLength()
	{
		$this->assertCreateFromResource(0, 5);
	}

	public function testWithOffsetAndLength()
	{
		$this->assertCreateFromResource(5, 10);
	}

	public function testToStringWithError()
	{
		$exception = null;

		try {
			Immutable::toString($this->toResource(), 0, null, true);
		} catch (RuntimeException $exception) {
			$this->assertEquals(Immutable::MESSAGE, $exception->getMessage());
		}

		$this->assertNotNull($exception);
	}

	private function assertCreateFromResource(
			int $start = 0,
			int $length = null
	) {
		$resource = $this->toResource();
		$size = $length ?? ($resource->size() - $start);
		$immutable = Immutable::createFromResource($resource, $start, $length);

		$expected = Immutable::toString($resource, $start, $length);
		$actual = Immutable::toString($immutable);

		$this->assertTrue($immutable->modified() >= $resource->modified());
		$this->assertEquals($size, $immutable->size());
		$this->assertEquals($resource->type(), $immutable->type());
		$this->assertEquals($expected, $actual);
	}

	private function toResource(): Resource
	{
		return new File(__FILE__);
	}
}
