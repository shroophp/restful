<?php

namespace ShrooPHP\RESTful\Tests\Resources;

use DateTime;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Bufferers\Bufferer;
use ShrooPHP\Core\Openable;
use ShrooPHP\RESTful\Resource\Traits\Pointer\Exception;
use ShrooPHP\RESTful\Resources\Immutable;
use ShrooPHP\RESTful\Resources\Pointer;
use org\bovigo\vfs\vfsStream;

class PointerTest extends TestCase implements Openable
{
	public function open()
	{
		return fopen(__FILE__, 'rb') ?: null;
	}

	public function test()
	{
		$this->assertPointer();
	}

	public function testWithArguments()
	{
		$this->assertPointer(
				'text/plain',
				DateTime::createFromFormat('U', 0),
				10
		);
	}

	public function testException()
	{
		$exception = null;
		$openable = new class implements Openable {

			public function open()
			{
				return null;
			}
		};
		$pointer = new Pointer($openable);

		try {
			$pointer->render(0, 0);
		} catch (Exception $exception) {
			// Do nothing (implicitly assign the exception).
		}

		$this->assertNotNull($exception);
	}

	private function assertPointer(
			string $type = null,
			DateTime $modified = null,
			int $size = null
	) {
		$root = vfsStream::setup();
		$openable = new Pointer($this, $type, $modified, $size);
		$synchronized = is_null($modified)
				? new DateTime <= $openable->modified()
				: $modified == $openable->modified();

		$this->assertEquals($type, $openable->type());
		$this->assertEquals($size, $openable->size());
		$this->assertTrue($synchronized);

		$handle = $this->open();
		$this->assertNotNull($handle);

		$contents = stream_get_contents($handle);
		fclose($handle);

		$this->assertContents($openable, $contents);
		$this->assertContents($openable, $contents, 5);
		$this->assertContents($openable, $contents, 0, 5);
		$this->assertContents($openable, $contents, 5, 10);
	}

	private function assertContents(
			Pointer $openable,
			string $contents,
			int $start = 0,
			int $length = null
	) {

		$expected = is_null($length)
				? substr($contents, $start)
				: substr($contents, $start, $length);

		$actual = Immutable::toString($openable, $start, $length);
		$this->assertEquals($expected, $actual);
	}
}
