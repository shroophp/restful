<?php

namespace ShrooPHP\RESTful\Tests\Resources;

use DateTime;
use ShrooPHP\RESTful\Resources\File;
use ShrooPHP\RESTful\Resources\Immutable;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
	public function testModified()
	{
		$path = $this->toPath();
		$file = new File($path);
		$modified = DateTime::createFromFormat('U', filemtime($path));

		$this->assertEquals($modified, $file->modified());
	}

	public function testRender()
	{
		$this->assertRender();
	}

	public function testRenderWithOffset()
	{
		$this->assertRender(5);
	}

	public function testRenderWithLength()
	{
		$this->assertRender(0, 5);
	}

	public function testRenderWithOffsetAndLength()
	{
		$this->assertRender(5, 10);
	}

	public function testSize()
	{
		$path = $this->toPath();
		$file = new File($path);

		$this->assertEquals(filesize($path), $file->size());
	}

	public function testType()
	{
		$path = $this->toPath();
		$file = new File($path);

		$finfo = finfo_open(FILEINFO_MIME);
		$expected = finfo_file($finfo, $path);
		finfo_close($finfo);

		$this->assertEquals($expected, $file->type());
	}

	private function assertRender(int $start = 0, int $length = null)
	{
		$path = $this->toPath();
		$file = new File($path);
		$contents = file_get_contents($path);
		$expected = is_null($length)
				? substr($contents, $start)
				: substr($contents, $start, $length);
		$actual = Immutable::toString($file, $start, $length);

		$this->assertEquals($expected, $actual);
	}

	private function toPath(): string
	{
		return __FILE__;
	}
}
