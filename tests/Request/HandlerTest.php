<?php

namespace ShrooPHP\RESTful\Tests\Request;

use DateInterval;
use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Openable;
use ShrooPHP\Core\ImmutableArrayObject;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\PSR\Request\Response\Presenters\BufferPresenter;
use ShrooPHP\RESTful\Request\Handler;
use ShrooPHP\RESTful\Collection;
use ShrooPHP\RESTful\Collection\Traits\Unimplemented;
use ShrooPHP\RESTful\Collections\HashTable;
use ShrooPHP\RESTful\Resource;
use ShrooPHP\RESTful\Resource\Traits\Unranged;
use ShrooPHP\RESTful\Resources\File;
use ShrooPHP\RESTful\Resources\Immutable;
use ShrooPHP\Core\Bufferer as IBufferer;
use ShrooPHP\Core\Bufferers\Bufferer;
use org\bovigo\vfs\vfsStream;

class HandlerTest extends TestCase implements Openable
{
	public function open()
	{
		return fopen(__FILE__, 'rb') ?: null;
	}

	public function testHandlePost()
	{
		$this->assertHandle('POST');
	}

	public function testHandlePut()
	{
		$this->assertHandle('PUT');
	}

	public function testHandlePatch()
	{
		$this->assertHandle('PATCH');
	}

	public function testHandleCreated()
	{
		$location = null;
		$path = '/created';
		$presenter = new BufferPresenter;
		$collection = new class($path) implements Collection {

			use Unimplemented;

			private $path;

			public function __construct(string $path)
			{
				$this->path = $path;
			}

			public function post(string $id, Resource $resource): ?string
			{
				return $this->path;
			}
		};
		$handler = new Handler($collection, $presenter);

		$this->assertTrue($handler->handle(new Request('POST', '/')));

		$response = $presenter->response();
		$this->assertNotNull($response);

		$this->assertEquals(Response::HTTP_CREATED, $response->code());

		foreach ($response->headers() as $header => $value) {

			switch ($header) {
				case 'Location':
					$location = $value;
					break;
			}
		}

		$this->assertEquals($path, $location);
	}

	public function testToCallable()
	{
		$handler = new Handler(new HashTable);
		$this->assertEquals([$handler, 'respond'], $handler->toCallable());
	}

	public function testHandleRanged()
	{
		$this->assertHandleRangeSince(5, 10);
	}

	public function testHandleUnmodified()
	{
		$this->assertHandleRangeSince(0, null, false);
	}

	public function testHandleModified()
	{
		$this->assertHandleRangeSince(0, null, true);
	}

	public function testHandleOffsetUnmodified()
	{
		$this->assertHandleRangeSince(1, null, false);
	}

	public function testHandleOffsetModified()
	{
		$this->assertHandleRangeSince(1, null, true);
	}

	public function testHandleRangedUnmodified()
	{
		$this->assertHandleRangeSince(5, 10, false);
	}

	public function testHandleRangedModified()
	{
		$this->assertHandleRangeSince(5, 10, true);
	}

	public function testHandleUnsupportedMethodGet()
	{
		$this->assertHandleUnsupportedMethod('GET');
	}


	public function testHandleUnsupportedMethodPost()
	{
		$this->assertHandleUnsupportedMethod('POST');
	}

	public function testHandleUnsupportedMethodPut()
	{
		$this->assertHandleUnsupportedMethod('PUT');
	}

	public function testHandleUnsupportedMethodPatch()
	{
		$this->assertHandleUnsupportedMethod('PATCH');
	}

	public function testHandleUnsupportedMethodDelete()
	{
		$this->assertHandleUnsupportedMethod('DELETE');
	}

	public function testHandleUnsupportedRange()
	{
		$unexpected = null;
		$id = '/';
		$collection = new HashTable;
		$presenter = new BufferPresenter;
		$handler = new Handler($collection, $presenter);
		$resource = new class implements Resource {

			use Unranged;

			protected function passthru()
			{
				// Do nothing.
			}

			public function modified(): DateTime
			{
				return new DateTime;
			}

			public function size(): ?int
			{
				return null;
			}

			public function type(): ?string
			{
				return null;
			}
		};

		$collection->put($id, $resource);

		$this->assertTrue($handler->handle(new Request('GET', $id)));

		$response = $presenter->response();
		$this->assertNotNull($response);

		foreach ($response->headers() as $header => $value) {

			$this->assertNotEquals('accept-bytes', strtolower(trim($header)));
		}
	}

	public function testHandleLengthOverflow()
	{
		$size = null;
		$range = null;
		$id = '/';
		$contents = 'Hello, world!';
		$offset = 7;
		$length = strlen($contents);
		$end = $length - 1;
		$headers = ['Range' => "bytes={$offset}-{$length}"];
		$request = new Request('GET', $id);
		$resource = new Immutable($contents, 'text/plain');
		$collection = new HashTable;
		$presenter = new BufferPresenter;
		$handler = new Handler($collection, $presenter);
		$bufferer = $this->toBufferer();

		$collection->put($id, $resource);

		$request->setHeaders(new ImmutableArrayObject($headers));

		$this->assertTrue($handler->handle($request));

		$response = $presenter->response();
		$this->assertNotNull($response);

		$handle = $bufferer->buffer($response->content());
		$expected = substr($contents, $offset);
		$actual = stream_get_contents($handle);
		fclose($handle);

		$this->assertEquals($expected, $actual);

		foreach ($response->headers() as $header => $value) {

			switch (strtolower(trim($header))) {
				case 'content-range':
					$range = $value;
					break;
				case 'content-length':
					$size = $value;
					break;
			}
		}

		$this->assertEquals("bytes {$offset}-{$end}/{$length}", $range);
		$this->assertEquals($length - $offset, $size);
	}

	private function assertHandle(string $method)
	{
		$id = '/';
		$expected = file_get_contents(__FILE__);
		$request = new Request('', $id);
		$collection = new HashTable;
		$presenter = new BufferPresenter;
		$handler = new Handler($collection, $presenter);
		$bufferer = $this->toBufferer();

		$request->setMethod('GET');
		$this->assertFalse($handler->handle($request));
		$this->assertNull($presenter->response());

		$request->setMethod($method);
		$request->setBody($this);
		$this->assertTrue($handler->handle($request));

		$response = $presenter->response();
		$this->assertNotNull($response);

		$content = $response->content();
		$this->assertNotNull($content);

		$actual = $bufferer->buffer($content);
		$this->assertEquals($expected, stream_get_contents($actual));

		$request->setMethod('DELETE');
		$this->assertTrue($handler->handle($request));
		$this->assertNull($collection->get($id));

		$presenter->clear();
	}

	private function assertHandleRangeSince(
			int $start = 0,
			int $length = null,
			bool $modified = null
	) {

		$since = null;
		$format = 'D, d M Y H:i:s';
		$gmt = "{$format} \G\M\T";
		$headers = [];
		$expected = [];
		$end = is_null($length) ? '' : (string) $start + $length;
		$utc = new DateTimeZone('UTC');
		$request = new Request('GET', '/');
		$resource = new File(__FILE__);
		$presenter = new BufferPresenter;
		$mtime = DateTime::createFromFormat('U', filemtime(__FILE__));
		$collection = new class($resource) implements Collection {

			use Unimplemented;

			private $resource;

			public function __construct(Resource $resource)
			{
				$this->resource = $resource;
			}

			public function get(string $id): ?Resource
			{
				return $this->resource;
			}
		};
		$handler = new Handler($collection, $presenter);
		$bufferer = $this->toBufferer();

		$headers['Range'] = "bytes={$start}-{$end}/*";

		$this->assertNotFalse($mtime);

		if (!is_null($modified)) {
			$since = clone $mtime;
			$this->assertNotFalse($since);
		}

		if (!is_null($since)) {

			if ($modified) {
				$since->sub(new DateInterval('PT1S'));
			}

			$headers['If-Modified-Since'] = $since->format("{$format} T");
		}

		$request->setHeaders(new ImmutableArrayObject($headers));

		$this->assertTrue($handler->handle($request));

		$response = $presenter->response();
		$this->assertNotNull($response);

		$expected['Pragma'] = 'no-cache';
		$expected['Cache-Control'] = 'no-cache';
		$expected['Last-Modified'] = $mtime->setTimezone($utc)->format($gmt);

		$content = $response->content();

		if (false === $modified) {

			$this->assertEquals(Response::HTTP_NOT_MODIFIED, $response->code());
			$this->assertNull($content);

		} else {

			$end = null;
			$size = $resource->size();
			$total = (string) ($size ?? '*');
			$sublength = $length ?? (($size ?? $start) - $start);
			$expected['Accept-Ranges'] = 'bytes';
			$expected['Content-Type'] = $resource->type();

			if (is_null($length)) {

				if (!is_null($size)) {
					$end = $size - 1;
				}

			} else {

				$expected['Content-Length'] = $sublength;
				$end = $start + $sublength -1;
			}

			if (!is_null($end)) {

				if ($end < 0) {
					$end = 0;
				}

				$expected['Content-Range'] = "bytes {$start}-{$end}/{$total}";
			}

			$this->assertNotNull($content);

			$string = Immutable::toString($resource, $start, $length);
			$contents = stream_get_contents($bufferer->buffer($content));
			$this->assertEquals($string, $contents);
		}

		$this->assertEquals($expected, iterator_to_array($response->headers()));
	}

	private function assertHandleUnsupportedMethod(string $method)
	{

		$collection = new class implements Collection {

			use Unimplemented;
		};
		$presenter = new BufferPresenter;
		$handler = new Handler($collection, $presenter);

		$this->assertTrue($handler->handle(new Request($method, '/')));

		$response = $presenter->response();
		$this->assertNotNull($response);

		$this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $response->code());
		$this->assertEquals([], iterator_to_array($response->headers()));
		$this->assertNull($response->content());
	}

	private function toBufferer(): IBufferer
	{
		$root = vfsStream::setup();
		return new Bufferer("{$root->url()}/buffer");
	}
}
