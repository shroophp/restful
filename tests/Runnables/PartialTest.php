<?php

namespace ShrooPHP\RESTful\Tests\Runnables;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Bufferers\Bufferer;
use ShrooPHP\RESTful\Runnables\Partial;
use ShrooPHP\RESTful\Resources\Immutable;

class PartialTest extends TestCase
{
	const CONTENTS = 'Hello, world!';

	public function testRun()
	{
		$this->assertPartial(self::CONTENTS);
	}

	public function testRunWithOffset()
	{
		$this->assertPartial(self::CONTENTS, 7);
	}

	public function testRunWithLength()
	{
		$this->assertPartial(self::CONTENTS, 0, 5);
	}

	public function testRunWithOffsetAndLength()
	{
		$this->assertPartial(self::CONTENTS, 7, 5);
	}

	private function assertPartial(
			string $contents,
			int $start = 0,
			int $length = null
	) {
		$root = vfsStream::setup();
		$bufferer = new Bufferer("{$root->url()}/buffer");
		$partial = $this->toPartial($contents, $start, $length);

		$expected = is_null($length)
				? substr($contents, $start)
				: substr($contents, $start, $length);
		$actual = stream_get_contents($bufferer->buffer($partial));

		$this->assertEquals($expected, $actual);
	}

	private function toPartial(
			string $contents,
			int $start = 0,
			int $length = null
	) {
		return new Partial(new Immutable($contents), $start, $length);
	}
}
